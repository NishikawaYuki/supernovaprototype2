﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField]
    GameObject m_GObjPlayer;

    Vector3 m_vPlayerPos;
    Vector3 m_vPlayerOldPos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_vPlayerOldPos = m_vPlayerPos;
        m_vPlayerPos = m_GObjPlayer.transform.localPosition;

        transform.localPosition += m_vPlayerPos - m_vPlayerOldPos;
    }
}
