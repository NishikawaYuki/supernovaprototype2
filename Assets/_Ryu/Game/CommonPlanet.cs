﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonPlanet : MonoBehaviour
{
    public enum COLORTYPE
    {
        CT_BLUE,
        CT_YELLOW,
        CT_RED,
        CT_MAX,
    };

    COLORTYPE m_ColorType;

    [SerializeField]
    float m_fMass;

    float m_fMassPlayer;

    Player m_Player;

    MeshRenderer m_MeshRenderer;

    float m_fLimitYellow = 1.0f;

    float m_fLimitRed = 2.0f;
    
    // Start is called before the first frame update
    void Start()
    {
        GameObject Obj = GameObject.Find("Player");
        m_Player = Obj.GetComponent<Player>();

        // メッシュレンダーラーを取得
        m_MeshRenderer = GetComponent<MeshRenderer>();
        m_MeshRenderer.material.EnableKeyword("_EMISSION");

        // 色の初期化
        m_ColorType = COLORTYPE.CT_BLUE;
        m_MeshRenderer.material.SetColor("_EmissionColor", new Color(0, 0.5f, 0.69f, 1));
    }

    // Update is called once per frame
    void Update()
    {
        m_fMassPlayer = m_Player.Weight();

        if (m_fMassPlayer <= 0.0f)
            return;

        if (m_fMass / m_fMassPlayer >= m_fLimitRed)
        {
            m_MeshRenderer.material.SetColor("_EmissionColor", new Color(1, 0, 0, 1));
            m_ColorType = COLORTYPE.CT_RED;
            return;
        }

        if (m_fMass / m_fMassPlayer >= m_fLimitYellow)
        {
            m_MeshRenderer.material.SetColor("_EmissionColor", new Color(1, 1, 0, 1));
            m_ColorType = COLORTYPE.CT_YELLOW;
            return;
        }

        m_MeshRenderer.material.SetColor("_EmissionColor", new Color(0, 0.5f, 0.69f, 1));
        m_ColorType = COLORTYPE.CT_BLUE;
    }

    public COLORTYPE ColorType()
    {
        return m_ColorType;
    }

    public float Mass()
    {
        return m_fMass;
    }
}
