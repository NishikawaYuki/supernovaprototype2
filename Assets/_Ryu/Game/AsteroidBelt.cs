﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBelt : MonoBehaviour
{
    [SerializeField]
    GameObject m_Prefab;

    [SerializeField]
    int m_nLength;

    [SerializeField]
    float m_fRangeOfRandomPos;

    [SerializeField]
    float m_fRandomScaleMin;

    [SerializeField]
    float m_fRandomScaleMax;

    Asteroid[] m_Asteroids;

    // Start is called before the first frame update
    void Start()
    {
        // プレイヤーを探す
        GameObject ObjPlayer = GameObject.Find("Player");

        m_Asteroids = new Asteroid[m_nLength];

        for (int nCount = 0; nCount < m_nLength; nCount++)
        {
            GameObject obj = Instantiate(m_Prefab, new Vector3(0, 0, 0), Quaternion.identity);

            m_Asteroids[nCount] = obj.GetComponent<Asteroid>();
            m_Asteroids[nCount].m_GObjPlayer = ObjPlayer;

            obj.transform.parent = transform;
            obj.transform.localPosition = new Vector3(Random.Range(-m_fRangeOfRandomPos, m_fRangeOfRandomPos), Random.Range(-m_fRangeOfRandomPos, m_fRangeOfRandomPos), 0.75f * nCount);

            float fScale = Random.Range(m_fRandomScaleMin, m_fRandomScaleMax);
            obj.transform.localScale = new Vector3(fScale, fScale, fScale);

          
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
