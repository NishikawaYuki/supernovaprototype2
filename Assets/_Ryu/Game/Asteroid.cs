﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField]
    int m_fMass;
    bool m_bIsHit;

    [SerializeField]
    float m_fLimitOfAttract;

    int m_nCount;
    int m_nCountMax = 10;

    // プレイヤー
    public GameObject m_GObjPlayer;

    // Start is called before the first frame update
    void Start()
    {
        m_bIsHit = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_bIsHit && (m_GObjPlayer.transform.position - transform.position).sqrMagnitude <= m_fLimitOfAttract * m_fLimitOfAttract)
        {
            m_bIsHit = true;
        }

        if (m_bIsHit && m_nCount < m_nCountMax)
        {
            m_nCount++;

            float a = m_nCount;
            float b = m_nCountMax;

            transform.position = transform.position + (m_GObjPlayer.transform.position - transform.position) * (a / b);
        }
    }

    public int GetMass()
    {
        return m_fMass;
    }
}

