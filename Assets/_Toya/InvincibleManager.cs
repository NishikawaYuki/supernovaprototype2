﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibleManager : MonoBehaviour {

    InvincibleGauge ig;
    Player player;

    public float _timer;            // タイマー
    public float _invincible;       // 無敵タイマー
    public float _invincibleGauge;  // 無敵ゲージ

    public bool _enable;            // 無敵中か

    private const int MAX_INVINCIBLE_GAUGE = 150;    // 無敵ゲージの最大フレーム
    private const float INVINCIBLE_TIME = 300;       // 無敵フレーム

    // Use this for initialization
    void Start () {

        ig = gameObject.GetComponent<InvincibleGauge>();

        _invincibleGauge = 0;
        _invincible = 0;
        _enable = false;
		
	}

    // Update is called once per frame
    void Update()
    {

        _timer += Time.deltaTime;


        // 経過時間と無敵ゲージを同期
        _invincibleGauge++;

        if (_invincibleGauge > MAX_INVINCIBLE_GAUGE)
        {// 無敵ゲージがMAX時
            _enable = true;
            _invincible++;
        }

        // 無敵が切れた
        if (_invincible > INVINCIBLE_TIME)
        {
            _enable = false;
            _invincible = 0.0f;
            _invincibleGauge = 0;
        }

    }
}


