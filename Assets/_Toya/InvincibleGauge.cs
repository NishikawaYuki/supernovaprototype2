﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibleGauge : MonoBehaviour {

    public float x;
    RectTransform trans;
 

	// Use this for initialization
	void Start () {
        x = -150;

        trans = GameObject.Find("Gauge").GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update () {

        bool enable =  gameObject.GetComponent<InvincibleManager>()._enable;
        trans.localPosition = new Vector3(x, 0, 0);
      
        if(enable)
        {
            SetStart();
        }
        else { Move(1.0f); }

    }

    public void SetStart() {

        x = -150;
    }

    public void Move(float f) {
        x += f;
    }
}
