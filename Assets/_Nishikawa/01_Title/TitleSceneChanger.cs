﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleSceneChanger : MonoBehaviour 
{
	void Update ()
    {
        if (Input.anyKeyDown) 
        {
            SceneNavigator.Instance.Change(Scene.Game);
            //AudioManager.Instance.PlaySE(SE.Select);
        }

    }
}
