﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blink : MonoBehaviour
{ 
    Text pressAnyKey;
    Color defaultColor;
    float t;

	void Start ()
    {
        pressAnyKey = GetComponent<Text>();
        defaultColor = pressAnyKey.color;
    }
	
	void Update () 
    {
        t += Time.deltaTime;
        pressAnyKey.color = new Color(defaultColor.r, defaultColor.g, defaultColor.b, 1 + Mathf.Sin(3.75f * t));
	}
}
