﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : SingletonMonoBehaviour<GameController>
{
    //シーンが開始される前に呼び出される
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void OnBeforeSceneLoadRuntimeMethod()
    {
        //解像度設定
        Screen.SetResolution(1280, 720, false, 60);

        //ゲームマネージャの生成
        GameObject game_manager = new GameObject("GameController");
        game_manager.AddComponent<GameController>();

        //シーン遷移管理クラスの生成
        new GameObject().AddComponent<SceneNavigator>();

        //オーディオの管理クラスの生成
        GameObject audio_manager = new GameObject("AudioManager");
        AudioSource bgm = audio_manager.AddComponent<AudioSource>();
        bgm.loop = true;
        bgm.playOnAwake = false;
        audio_manager.AddComponent<AudioManager>();
        audio_manager.AddComponent<AudioListener>();
    }

    public void Start()
    {
        DontDestroyOnLoad(this);
    }
}
