﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum BGM
{
    None = -1,
    Title,
    Play,
    Result,
}

public enum SE
{
    None = -1,
    Select,
    Cancel,
}

public class AudioManager : SingletonMonoBehaviour<AudioManager>
{
    //ボリューム保存用のkeyとデフォルト値
    const string BGM_VOLUME_KEY = "BGM_VOLUME_KEY";
    const string SE_VOLUME_KEY = "SE_VOLUME_KEY";
    const float BGM_VOLUME_DEFULT = 1.0f;
    const float SE_VOLUME_DEFULT = 1.0f;

    //BGMがフェードするのにかかる時間
    public const float BGM_FADE_SPEED_RATE_HIGH = 0.9f;
    public const float BGM_FADE_SPEED_RATE_LOW = 0.3f;
    float bgm_fade_speed_rate = BGM_FADE_SPEED_RATE_HIGH;

    //次流すBGM名、SE名
    string next_bgm_name;
    string next_se_name;

    //BGMをフェードアウト中か
    bool fade_out = false;

    //BGM用、SE用に分けてオーディオソースを持つ
    AudioSource bgm;

    //全Audioを保持
    Dictionary<string, AudioClip> bgm_dic, se_dic;

    //再生中のSEをまとめておくためのオブジェクト
    GameObject se_group;

    Dictionary<BGM, string> bgm_name;
    Dictionary<SE, string>  se_name;

    override protected void Init()
    {
        bgm_name = new Dictionary<BGM, string>()
        {
            {BGM.Title,     "title"},
            {BGM.Play ,     "play"},
            {BGM.Result,    "result"},
        };

        se_name = new Dictionary<SE, string>()
        {
            {SE.Select,         "select" },
            {SE.Cancel,         "cancel" },
        };
    }

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        //リソースフォルダから全SE&BGMのファイルを読み込みセット
        bgm_dic = new Dictionary<string, AudioClip>();
        se_dic = new Dictionary<string, AudioClip>();

        object[] bgm_list = Resources.LoadAll("Audio/Bgm");
        object[] se_list = Resources.LoadAll("Audio/Se");

        foreach (AudioClip bgm in bgm_list)
        {
            bgm_dic[bgm.name] = bgm;
        }
        foreach (AudioClip se in se_list)
        {
            se_dic[se.name] = se;
        }

        bgm = GetComponent<AudioSource>();
        bgm.volume = PlayerPrefs.GetFloat(BGM_VOLUME_KEY, BGM_VOLUME_DEFULT);
        bgm.Stop();

        se_group = new GameObject();
        se_group.name = "SE";
        se_group.transform.SetParent(transform);
    }

    public void PlaySE(SE id, float volume = 1.0f)
    {
        if (!se_dic.ContainsKey(se_name[id]))
        {
            Debug.Log(se_name[id] + "という名前のSEがありません");
            return;
        }

        GameObject obj = new GameObject("playing_se");
        obj.transform.SetParent(se_group.transform);
        AudioSource se = obj.AddComponent<AudioSource>();
        se.playOnAwake = false;
        se.volume = volume;
        PlayerPrefs.GetFloat(SE_VOLUME_KEY, volume);
        se.PlayOneShot(se_dic[se_name[id]] as AudioClip);
        StartCoroutine(EndCheck(obj, se));
    }

    IEnumerator EndCheck(GameObject obj, AudioSource se)
    {
        while (true)
        {
            if (se)
            {
                if (!se.isPlaying)
                {
                    Destroy(obj);
                }
            }

            yield return new WaitForFixedUpdate();
        }
    }

    public void PlayBGM(BGM id, float fadeSpeedRate = BGM_FADE_SPEED_RATE_HIGH)
    {
        
        if (!bgm_dic.ContainsKey(bgm_name[id]))
        {
            Debug.Log(bgm_name[id] + "という名前のBGMがありません");
            return;
        }

        //現在BGMが流れていない時はそのまま流す
        if (!bgm.isPlaying)
        {
            next_bgm_name = "";
            bgm.clip = bgm_dic[bgm_name[id]] as AudioClip;
            bgm.Play();
        }
        //違うBGMが流れている時は、流れているBGMをフェードアウトさせてから次を流す。同じBGMが流れている時はスルー
        else if (bgm.clip.name != bgm_name[id])
        {
            next_bgm_name = bgm_name[id];
            FadeOutBGM(fadeSpeedRate);
        }

    }

    public void FadeOutBGM(float fade_speed_rate = BGM_FADE_SPEED_RATE_LOW)
    {
        bgm_fade_speed_rate = fade_speed_rate;
        fade_out = true;
    }

    private void Update()
    {
        if (!fade_out)
        {
            return;
        }

        //徐々にボリュームを下げていき、ボリュームが0になったらボリュームを戻し次の曲を流す
        bgm.volume -= Time.deltaTime * bgm_fade_speed_rate;
        if (bgm.volume <= 0)
        {
            bgm.Stop();
            bgm.volume = PlayerPrefs.GetFloat(BGM_VOLUME_KEY, BGM_VOLUME_DEFULT);
            fade_out = false;

            if (!string.IsNullOrEmpty(next_bgm_name))
            {
                PlayBGM(BGMByName(next_bgm_name));
            }
        }

    }

    public void ChangeVolume(float BGMVolume)
    {
        bgm.volume = BGMVolume;
        PlayerPrefs.SetFloat(BGM_VOLUME_KEY, BGMVolume);
    }

    private BGM BGMByName(string name)
    {
        BGM key = BGM.None;

        foreach (KeyValuePair<BGM, string> pair in bgm_name)
        {
            if (pair.Value == name)
            {
                key = pair.Key;
                break;
            }
        }
        return key;
    }
}