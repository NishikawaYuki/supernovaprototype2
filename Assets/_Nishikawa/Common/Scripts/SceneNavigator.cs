﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;

// シーンのリストをenumで作る
public enum Scene
{
    None = -1,
    Title,
    Game,
    Result,
    GameOver,
    Battle,
}

public class SceneNavigator : SingletonMonoBehaviour<SceneNavigator>
{
    // シーン名とenumのシーンとを対応させる
    static Dictionary<Scene, string> SceneName = new Dictionary<Scene, string>()
    {
        {Scene.Title ,  "01_Title"},
        {Scene.Game,    "02_Game"},
        {Scene.Result , "03_Result"},
        {Scene.GameOver, "04_GameOver"},
        {Scene.Battle, "05_Battle"}
    };

    //フェード中か否か
    public bool IsFading
    {
        get { return _fader.IsFading || _fader.Alpha != 0; }
    }

    //一個前と現在、次のシーン名
    private string _beforeSceneName = "";
    public string BeforeSceneName
    {
        get { return _beforeSceneName; }
    }

    private string _currentSceneName = "";
    public string CurrentSceneName
    {
        get { return _currentSceneName; }
    }

    private string _nextSceneName = "";
    public string NextSceneName
    {
        get { return _nextSceneName; }
    }

    //フェード後のイベント
    public event Action FadeOutFinished = delegate { };
    public event Action FadeInFinished = delegate { };

    //フェード用クラス
    [SerializeField]
    private CanvasFader _fader = null;

    //フェード時間
    public const float FADE_TIME = 0.5f;
    private float _fadeTime = FADE_TIME;

    Scene previous_ = Scene.None;
    Scene current_ = Scene.None;
    Scene next_ = Scene.None;
    Scene additive_scene_ = Scene.None;

    //=================================================================================
    //初期化
    //=================================================================================

    /// <summary>
    /// 初期化(Awake時かその前の初アクセス時、どちらかの一度しか行われない)
    /// </summary>
    protected override void Init()
    {
        base.Init();

        //実機上やエディタを実行している時にはAddした場合はResetが実行されないので、Initから実行
        if (_fader == null)
        {
            Reset();
        }

        //最初のシーン名設定
        _currentSceneName = SceneManager.GetSceneAt(0).name;
        current_ = SceneByName(SceneManager.GetSceneAt(0).name);

        //永続化し、フェード用のキャンバスを非表示に
        DontDestroyOnLoad(gameObject);
        _fader.gameObject.SetActive(false);
    }

    //コンポーネント追加時に自動で実行される(実機上やエディタを実行している時には動作しない)
    private void Reset()
    {
        //オブジェクトの名前を設定
        gameObject.name = "SceneNavigator";

        //フェード用のキャンバス作成
        GameObject fadeCanvas = new GameObject("FadeCanvas");
        fadeCanvas.transform.SetParent(transform);
        fadeCanvas.SetActive(false);

        Canvas canvas = fadeCanvas.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        canvas.sortingOrder = 999;

        fadeCanvas.AddComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        fadeCanvas.AddComponent<GraphicRaycaster>();
        _fader = fadeCanvas.AddComponent<CanvasFader>();
        _fader.Alpha = 0;

        //フェード用の画像作成
        GameObject imageObject = new GameObject("Image");
        imageObject.transform.SetParent(fadeCanvas.transform, false);
        imageObject.AddComponent<Image>().color = Color.black;
        imageObject.GetComponent<RectTransform>().sizeDelta = new Vector2(2000, 2000);
    }

    //=================================================================================
    //シーンの変更
    //=================================================================================

    /// <summary>
    /// シーンの変更
    /// </summary>
    public void Change(Scene scene, Scene additive_scene = Scene.None, float fadeTime = FADE_TIME)
    {
        if (IsFading) return;

        //次のシーン名とフェード時間を設定
        _nextSceneName = SceneName[scene];
        next_ = scene;
        _fadeTime = fadeTime;

        //追加のシーンがあれば保存
        additive_scene_ = additive_scene;

        //フェードアウト
        _fader.gameObject.SetActive(true);
        _fader.Play(isFadeOut: false, duration: _fadeTime, onFinished: OnFadeOutFinish);
    }

    public Scene CurrentScene()
    {
        return current_;
    }

    public bool IsCurrentScene(Scene current)
    {
        return current_ == current;
    }

    //フェードアウト終了
    private void OnFadeOutFinish()
    {
        FadeOutFinished();

        //シーン読み込み、変更
        SceneManager.LoadScene(_nextSceneName);

        if (additive_scene_ != Scene.None)
            SceneManager.LoadScene(SceneName[additive_scene_], LoadSceneMode.Additive);
       
        //シーン名更新
        _beforeSceneName = _currentSceneName;
        _currentSceneName = _nextSceneName;

        previous_ = SceneByName(_currentSceneName);
        current_ = SceneByName(_nextSceneName);

        //フェードイン開始
        _fader.gameObject.SetActive(true);
        _fader.Alpha = 1;
        _fader.Play(isFadeOut: true, duration: _fadeTime, onFinished: OnFadeInFinish);
    }

    //フェードイン終了
    private void OnFadeInFinish()
    {
        _fader.gameObject.SetActive(false);
        FadeInFinished();
    }

    private Scene SceneByName(string scene_name)
    {
        Scene key = Scene.None;

        foreach (KeyValuePair<Scene, string> pair in SceneName)
        {
            if (pair.Value == scene_name)
            {
                key = pair.Key;
                break;
            }
        }
        return key;
    }

}