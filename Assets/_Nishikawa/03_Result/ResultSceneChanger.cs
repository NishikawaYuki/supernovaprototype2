﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultSceneChanger : MonoBehaviour 
{
	void Update ()
    {
        if (Input.anyKeyDown)
            SceneNavigator.Instance.Change(Scene.Title);
    }
}
