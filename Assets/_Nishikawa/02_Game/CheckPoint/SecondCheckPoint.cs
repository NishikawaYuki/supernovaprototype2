﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondCheckPoint : MonoBehaviour
{
    [SerializeField] int needWeight;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (other.gameObject.GetComponent<Player>().Weight() < needWeight)
            {
                SceneNavigator.Instance.Change(Scene.GameOver);
            }
        }
    }
}
