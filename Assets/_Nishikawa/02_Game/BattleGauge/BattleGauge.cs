﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleGauge : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject boss;
    [SerializeField] Image image;
    [SerializeField] Image center;
    [SerializeField] Image background;

    RectTransform rt;
    float rate;
    float timeLimit = 5.0f;

    void Start()
    {
        rate = 0.3f;
        rt = (RectTransform)image.transform;
    }

    void Update()
    {
        rate -= 0.002f;

        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            rate += 0.015f;
        }

        rate = Mathf.Clamp(rate, 0.0f, 1.0f);

        Rate(rate);

        if(timeLimit <= 0.0f) 
        {
            if (rate >= 0.5f)
            {
                Destroy(boss);
                SceneNavigator.Instance.Change(Scene.Result);
            }
            else
            {
                Destroy(player);
                SceneNavigator.Instance.Change(Scene.GameOver);
            }
        }
        timeLimit -= Time.deltaTime;
    }

    void Rate(float value) 
    {
        rt.localScale = new Vector3(value, 1.0f, 1.0f);
    }
}