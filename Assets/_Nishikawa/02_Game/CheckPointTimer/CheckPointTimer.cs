﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointTimer : MonoBehaviour
{
    const float CHECK_POINT_CREATE_POSITION_Z = 40;

    [SerializeField] float firstCheckPointTime;
    [SerializeField] float secondCheckPointTime;
    [SerializeField] float bossMoveTime;

    [SerializeField] GameObject firstCheckPoint;
    [SerializeField] GameObject secondCheckPoint;
    [SerializeField] GameObject boss;

    bool createFirstOnce = true;
    bool createSecondOnce = true;
    bool bossMoveOnce = true;

    float time = 0.0f;

    void Start()
    {
        
    }

    void Update()
    {
        if (time < firstCheckPointTime)
        {
            //なにもない
        }
        else if (firstCheckPointTime <= time && time < secondCheckPointTime)
        {
            if(createFirstOnce)
            {
                CreateFirstCheckPoint();
                createFirstOnce = false;
            }

        }
        else if (secondCheckPointTime <= time && time < bossMoveTime)
        {
            if (createSecondOnce)
            {
                CreateSecondCheckPoint();
                createSecondOnce = false;
            }
        }
        else if (bossMoveTime <= time) 
        {
            if (bossMoveOnce) 
            {
                BossStartMove();
                bossMoveOnce = false;
            }
        }

        time += Time.deltaTime;
    }

    void CreateFirstCheckPoint()
    {
        Instantiate(firstCheckPoint, new Vector3(0,0,CHECK_POINT_CREATE_POSITION_Z), Quaternion.identity);
    }

    void CreateSecondCheckPoint()
    {
        Instantiate(secondCheckPoint, new Vector3(0,0,CHECK_POINT_CREATE_POSITION_Z), Quaternion.identity);
    }

    void BossStartMove()
    {
        Rigidbody rb = boss.GetComponent<Rigidbody>();
        rb.AddForce(new Vector3(0.0f, 0.0f, -10.0f), ForceMode.Impulse);
    }
}
