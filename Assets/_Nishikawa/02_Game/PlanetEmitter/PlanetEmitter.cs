﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetEmitter : MonoBehaviour 
{
    enum PlanetColor 
    {
        Red,
        Yellow,
        Blue,
        Gimmick,
    }

    [SerializeField] GameObject planetBlue;
    [SerializeField] GameObject planetYellow;
    [SerializeField] GameObject planetRed;
    [SerializeField] GameObject planetGimmick;

    [Header("青星生成確率")]
    public float BlueCreateProbability = 0.3f;

    [Header("黄星生成確率")]
    [Range(0, 1.0f)]
    public float YellowCreateProbability = 0.3f;

    [Header("赤星生成確率")]
    [Range(0, 1.0f)]
    public float RedCreateProbability = 0.3f;

    [Header("連鎖生成確率")]
    public float GimmickCreateProbability = 0.1f;

    [Header("敵惑星最小生成時間")]
    public float CreateTime = 0.3f;

    [Header("敵惑星生成時間間隔")]
    public float CreateTimeInterval = 0.2f;


    Dictionary<PlanetColor, GameObject> planets;

    List<GameObject> existPlanets;

    float nextTime;
    float time;

    bool stopEmit;

    void Start () 
    {
        planets = new Dictionary<PlanetColor, GameObject>();
        planets.Add(PlanetColor.Red,    planetRed);
        planets.Add(PlanetColor.Yellow, planetYellow);

        planets.Add(PlanetColor.Blue,   planetBlue);
        planets.Add(PlanetColor.Gimmick, planetGimmick);

        planets.Add(PlanetColor.Blue,   planetBlue);
        planets.Add(PlanetColor.Gimmick, planetGimmick);

        existPlanets = new List<GameObject>();

    }
	
	void Update () 
    {
        if (stopEmit) return;

        //普通の生成間隔
        if (time >= nextTime)
        {
            float rate = Random.Range(0.0f, 1.0f);

            if (0.0f <= rate && rate < BlueCreateProbability)
            {
                Emit(PlanetColor.Blue);
            }
            else if (BlueCreateProbability <= rate && rate < (BlueCreateProbability + YellowCreateProbability))
            {
                Emit(PlanetColor.Yellow);
            }
            else if (0.7f <= rate && rate < 0.9f)
            {
                Emit(PlanetColor.Gimmick);
            }
            else if ((BlueCreateProbability + YellowCreateProbability) <= rate && rate < (BlueCreateProbability + YellowCreateProbability + GimmickCreateProbability))
            {
                Emit(PlanetColor.Gimmick);
            }
            else
            {
                Emit(PlanetColor.Red);
            }

            nextTime = Random.Range(CreateTime, CreateTime + CreateTimeInterval);
            time = 0.0f;
        }

        time += Time.deltaTime;
    }

    void Emit(PlanetColor color)
    {
        Vector3 position;
        position.x = Random.Range(-10, 10);
        position.y = Random.Range(-5, 5);
        position.z = 40;

        existPlanets.Add(Instantiate(planets[color], position, Quaternion.identity));
    }

    public void StopEmit() 
    {
        stopEmit = true;
    }

    public void RemoveFieldPlanetALL() 
    {
        foreach(var ep in existPlanets) 
        {
            if(ep)
                Destroy(ep.gameObject);
        }
    }
}
