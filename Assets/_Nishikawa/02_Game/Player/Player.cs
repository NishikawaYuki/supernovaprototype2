﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour 
{
    Rigidbody rb;

    int weight = 30;
    [SerializeField] Text weightText;
    int playerCount = 3;
    [SerializeField] Text playerCountText;

    void Start () 
    {
        rb = GetComponent<Rigidbody>();
    }
	
	void Update ()
    {
        CalcWeight();
        CalcPlayerCount();

        Move();
    }

    void CalcWeight() 
    {
        weightText.text = weight.ToString();
    }

    void CalcPlayerCount()
    {
        playerCountText.text = playerCount.ToString();
    }

    void Move()
    {
        rb.MovePosition(rb.position + new Vector3(0.0f, 0.0f, 0.8f));

        if (Input.GetKey(KeyCode.A))
        {
            //rb.AddForce(new Vector3(-0.3f, 0.0f, 0.0f), ForceMode.Impulse);
            rb.MovePosition(rb.position + new Vector3(-0.1f, 0.0f, 0.0f));
        }
        if (Input.GetKey(KeyCode.D))
        {
            //rb.AddForce(new Vector3(0.3f, 0.0f, 0.0f), ForceMode.Impulse);
            rb.MovePosition(rb.position + new Vector3(0.1f, 0.0f, 0.0f));
        }
        if (Input.GetKey(KeyCode.W))
        {
            //rb.AddForce(new Vector3(0.0f, 0.3f, 0.0f), ForceMode.Impulse);
            rb.MovePosition(rb.position + new Vector3(0.0f, 0.1f, 0.0f));
        }
        if (Input.GetKey(KeyCode.S))
        {
            //rb.AddForce(new Vector3(0.0f, -0.3f, 0.0f), ForceMode.Impulse);
            rb.MovePosition(rb.position + new Vector3(0.0f, -0.1f, 0.0f));
        }
    }

    public int Weight() 
    {
        return weight;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("CommonPlanet"))
        {
            CommonPlanet commonPlanet = other.gameObject.GetComponent<CommonPlanet>();

            switch(commonPlanet.ColorType())
            {
                case CommonPlanet.COLORTYPE.CT_BLUE:
                    {
                        // エナジーゲージをチャージ

                        // 惑星を消滅
                        Destroy(other.gameObject);
                        break;
                    }
                case CommonPlanet.COLORTYPE.CT_YELLOW:
                    {
                        weight -= (int)commonPlanet.Mass() - weight;

                        // 惑星を消滅
                        Destroy(other.gameObject);
                        break;
                    }
                case CommonPlanet.COLORTYPE.CT_RED:
                    {
                        SceneNavigator.Instance.Change(Scene.GameOver);
                        break;
                    }
            }
        }

        if (other.gameObject.CompareTag("GreenPlanet"))
        {
            other.transform.parent.GetComponent<Gimmick>().Delete();

            playerCount -= 1;
            //カメラ中心に帰る
            gameObject.transform.position = new Vector3(0, 0, 0);
            Destroy(other.gameObject);
            if (playerCount <= 0)
            {
                SceneNavigator.Instance.Change(Scene.GameOver);
            }

        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Asteroid")) 
        {
            weight += other.gameObject.GetComponent<Asteroid>().GetMass();
            Destroy(other.gameObject);
        }

        if(other.gameObject.CompareTag("GreenPlanet"))
        {
            other.transform.parent.GetComponent<Gimmick>().Delete();
        }
    }
}
