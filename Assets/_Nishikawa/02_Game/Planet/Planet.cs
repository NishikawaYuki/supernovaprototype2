﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour 
{
    Rigidbody rb;

	void Start () 
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(new Vector3(0.0f, 0.0f, -8.0f), ForceMode.Impulse);
	}
	
	void Update () 
    {
		if(transform.position.z < -10) 
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("MoveLimit")) 
        {
            Destroy(gameObject);
        }
    }
}
