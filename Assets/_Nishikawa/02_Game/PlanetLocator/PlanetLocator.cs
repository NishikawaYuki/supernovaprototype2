﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetLocator : MonoBehaviour
{
    public enum Sector 
    {
        S1,
        S2,
        S3,
        S4,
        S5,
        S6,
    }

    public enum Planet
    {
        Asteroid,
        Planet,
        Bomb,
    }

    const float LIMIT_LEFT = -7.5f;
    const float LIMIT_RIGHT = 7.5f;
    const float WIDTH = 15.0f;
    const float HEIGHT = 10.0f;
    const float DEPTH = 200.0f;

    const int DIV_X = 10;
    const int DIV_Y = 3;
    const int DIV_Z = 10;
    
    [SerializeField] GameObject asteroid;
    [SerializeField] GameObject planet;
    [SerializeField] GameObject bomb;
    static Sector sector_;

    static Dictionary<Planet, GameObject> prefabs;

    void Awake()
    {
        prefabs = new Dictionary<Planet, GameObject>();
        prefabs.Add(Planet.Asteroid, asteroid);
        prefabs.Add(Planet.Planet, planet);
        prefabs.Add(Planet.Bomb, bomb);
    }

    static public void SetSector(Sector sector) 
    {
        sector_ = sector;
    }

    static public void Locate(Planet type, int x, int y, int z)
    {
        int dx = x - 1;
        int dy = y - 1;
        int dz = z - 1;

        float add_z = (float)sector_ * 200.0f;
        float px = -(WIDTH / 2.0f) + (((WIDTH / DIV_X)) / 2.0f) * (((dx + 1.0f) - (dx - 1.0f)) / 2.0f) + dx * ((WIDTH / DIV_X)) * (((dx + 1.0f) - (dx - 1.0f)) / 2.0f);
        float py = -(HEIGHT / 2.0f) + (((HEIGHT / DIV_Y)) / 2.0f) * (((dy + 1.0f) - (dy - 1.0f)) / 2.0f) + dy * ((HEIGHT / DIV_Y)) * (((dy + 1.0f) - (dy - 1.0f)) / 2.0f);
        float pz = add_z + (DEPTH) - (((DEPTH / DIV_Z)) / 2.0f) * (((dz + 1.0f) - (dz - 1.0f)) / 2.0f) - dz * ((DEPTH / DIV_Z)) * (((dz + 1.0f) - (dz - 1.0f)) / 2.0f);
        Instantiate(prefabs[type], new Vector3( px, py, pz), Quaternion.identity);
    }
}
