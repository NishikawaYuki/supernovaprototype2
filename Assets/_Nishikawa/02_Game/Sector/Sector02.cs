﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sector02 : MonoBehaviour
{
    void Start()
    {

        PlanetLocator.SetSector(PlanetLocator.Sector.S2);

        //小惑星
        //上
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 1, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 1, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 1, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 9, 1, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 1, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 1, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 1, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 1, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 1, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 9, 1, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 1, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 1, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 1, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 1, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 1, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 1, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 1, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 9, 1, 10);
        //中
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 2, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 2, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 2, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 2, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 4, 2, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 2, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 2, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 7, 2, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 1, 2, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 2, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 2, 10);
        //下
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 3, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 4, 3, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 7, 3, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 3, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 1, 3, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 10, 3, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 1, 3, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 3, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 9, 3, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 10, 3, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 3, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 3, 9);

        //青
        //上
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 1, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 1, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 1, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 1, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 1, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 1, 7);
        //中
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 2, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 2, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 2, 10);
        //下
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 3, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 3, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 3, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 3, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 3, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 3, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 3, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 3, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 3, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 3, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 3, 10);

        //黄色
        //上
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 1, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 1, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 1, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 1, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 1, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 1, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 1, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 1, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 1, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 1, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 1, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 1, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 1, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 1, 9);
        //中
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 2, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 2, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 2, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 2, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 2, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 2, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 2, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 2, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 2, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 2, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 2, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 2, 9);
        //下
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 3, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 3, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 3, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 3, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 3, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 3, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 3, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 3, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 3, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 3, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 3, 8);

        //赤
        //上
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 1, 10);
        //中
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 2, 10);
        //下
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 3, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 3, 10);
    }

}
