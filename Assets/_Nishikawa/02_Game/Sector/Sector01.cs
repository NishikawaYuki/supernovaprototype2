﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sector01 : MonoBehaviour
{
    void Start()
    {
        PlanetLocator.SetSector(PlanetLocator.Sector.S1);

        //小惑星
        //上
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 1, 1, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 1, 1, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 1, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 1, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 1, 4);

        //中
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 2, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 2, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 2, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 2, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 4, 2, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 7, 2, 3);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 2, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 2, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 2, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 2, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 2, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 2, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 4, 2, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 7, 2, 10);

        //下
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 3, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 9, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 10, 3, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 10, 3, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 9, 3, 8);
    }
}
