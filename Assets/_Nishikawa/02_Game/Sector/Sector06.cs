﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sector06 : MonoBehaviour
{
    void Start()
    {
        PlanetLocator.SetSector(PlanetLocator.Sector.S6);
        //小惑星
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 1, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 7, 1, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 1, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 7, 1, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 2, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 2, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 2, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 3, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 4, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 5, 3, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 4, 3, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 10, 3, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 3, 10);
        //星
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 1, 4);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 1, 4);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 1, 4);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 1, 5);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 1, 5);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 1, 5);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 1, 5);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 1, 5);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 1, 6);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 1, 6);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 1, 7);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 1, 7);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 1, 7);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 1, 8);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 1, 8);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 1, 8);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 1, 8);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 1, 8);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 1, 9);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 1, 9);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 1, 10);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 1, 10);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 1, 10);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 1, 10);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 2, 4);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 2, 4);  //青 
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 2, 4);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 2, 4);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 2, 5);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 2, 5);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 2, 5);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 2, 6);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 2, 6);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 2, 7);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 2, 7);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 2, 7);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 2, 7);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 2, 7);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 2, 8);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 2, 9);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 2, 9);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 2, 9);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 2, 10);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 2, 10);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 2, 10);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 3, 3);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 3, 3);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 4);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 3, 4);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 3, 4);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 3, 4);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 3, 5);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 3, 5);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 3, 5);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 3, 6);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 3, 6);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 3, 6);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 3, 7);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 3, 7);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 3, 7);  //赤 
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 8);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 3, 8);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 6, 3, 8);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 3, 9);  //金
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 3, 9);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 3, 9);  //青
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 10);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 3, 10);  //赤
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 3, 10);  //赤

        //爆発星
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 1, 1, 1);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 6, 1, 1);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 10, 1, 1);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 4, 1, 2);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 8, 1, 2);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 7, 1, 3);    //Start2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 10, 1, 9);    //1
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 2, 2, 1);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 7, 2, 1);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 5, 2, 2);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 9, 2, 2);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 3, 2, 3);    //Start2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 8, 2, 8);    //1
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 9, 2, 10);    //Start1
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 4, 3, 1);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 2, 3, 2);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 7, 3, 2);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 10, 3, 2);    //2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 5, 3, 3);    //Start2
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 8, 3, 8);    //1
        PlanetLocator.Locate(PlanetLocator.Planet.Bomb, 10, 3, 8);    //1
    }
}
