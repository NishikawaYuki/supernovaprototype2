﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sector03 : MonoBehaviour
{
    void Start()
    {
        PlanetLocator.SetSector(PlanetLocator.Sector.S3);

        // yellow -> 黄
        // red -> 赤
        // blue ->　青
        // small -> 小惑星 asteroid

        // 1段
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 1, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 2, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 2, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 9, 2, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 3, 1);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 1);
        //PlanetLocator.Locate.yellow(8, 1, 1);
        //PlanetLocator.Locate.yellow(7, 2, 1);
        //PlanetLocator.Locate.yellow(8, 2, 1);
        //PlanetLocator.Locate.yellow(9, 2, 1);
        //PlanetLocator.Locate.yellow(8, 3, 1);
        //PlanetLocator.Locate.red(1, 3, 1);

        // 2段
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 1, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 1, 2);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 2, 2);
        //PlanetLocator.Locate.red(5, 1, 2);
        //PlanetLocator.Locate.blue(8, 1, 2);
        //PlanetLocator.Locate.red(2, 2, 2);

        // 3段
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 2, 3);
        //PlanetLocator.Locate.yellow(8, 2, 3);

        // 4段
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 1, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 2, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 4, 2, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 10, 3, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 3, 4);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 3, 4);
        //PlanetLocator.Locate.yellow(3, 1, 4);
        //PlanetLocator.Locate.yellow(2, 2, 4);
        //PlanetLocator.Locate.yellow(3, 2, 4);
        //PlanetLocator.Locate.yellow(4, 2, 4);
        //PlanetLocator.Locate.red(10, 3, 4);
        //PlanetLocator.Locate.blue(8, 3, 4);
        //PlanetLocator.Locate.yellow(3, 3, 4);

        // 5段
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 2, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 5, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 3, 5);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 5);
        //PlanetLocator.Locate.yellow.(1, 2, 5);
        //PlanetLocator.Locate.red.   (5, 3, 5);
        //PlanetLocator.Locate.blue.  (3, 3, 5);
        //PlanetLocator.Locate.yellow.(1, 3, 5);

        // 6段
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 1, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 1, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 1, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 7, 2, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 8, 2, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 3, 2, 6);
        PlanetLocator.Locate(PlanetLocator.Planet.Planet, 1, 3, 6);
        //PlanetLocator.Locate.red   (7, 1, 6);
        //PlanetLocator.Locate.red   (8, 1, 6);
        //PlanetLocator.Locate.yellow(3, 1, 6);
        //PlanetLocator.Locate.red   (7, 2, 6);
        //PlanetLocator.Locate.red   (8, 2, 6);
        //PlanetLocator.Locate.blue  (3, 2, 6);
        //PlanetLocator.Locate.yellow(1, 3, 6);

        // 7段
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 7, 1, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 3, 1, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 10, 2, 7);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 2, 7);
        //PlanetLocator.Locate.small(7, 1, 7);
        //PlanetLocator.Locate.blue  (3, 1, 7);
        //PlanetLocator.Locate.yellow(10, 2, 7);
        //PlanetLocator.Locate.small(8, 2, 7);

        // 8段
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 8, 1, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 10, 2, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 7, 2, 8);
        //PlanetLocator.Locate.small(8, 1, 8);
        //PlanetLocator.Locate.yellow(10, 2, 8);
        //PlanetLocator.Locate.small(7, 2, 8);

        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 1, 2, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 2, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 1, 3, 8);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 3, 8);
        //PlanetLocator.Locate.red(1, 2, 8);
        //PlanetLocator.Locate.red(2, 2, 8);
        //PlanetLocator.Locate.red(1, 3, 8);
        //PlanetLocator.Locate.red(2, 3, 8);

        // 9段
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 6, 2, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 2, 9);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 1, 3, 9);
        //PlanetLocator.Locate.red(6, 2, 9);
        //PlanetLocator.Locate.small(2, 2, 9);
        //PlanetLocator.Locate.small(1, 3, 9);

        // 10段
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 1, 2, 10);
        PlanetLocator.Locate(PlanetLocator.Planet.Asteroid, 2, 3, 10);
        //PlanetLocator.Locate.small(1, 2, 10);
        //PlanetLocator.Locate.small(2, 3, 10);
    }
}
