﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverSceneChanger : MonoBehaviour 
{
    void Update()
    {
        if (Input.anyKeyDown)
        {
            SceneNavigator.Instance.Change(Scene.Title);
            //AudioManager.Instance.PlaySE(SE.Select);
        }
    }
}
