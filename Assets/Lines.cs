﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lines : MonoBehaviour
{
    [SerializeField] Transform target;

    void Update()
    {
       transform.position = new Vector3(0.0f, 0.0f, target.position.z);
    }
}
