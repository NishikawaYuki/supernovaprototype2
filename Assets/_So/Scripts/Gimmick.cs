﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gimmick : MonoBehaviour
{
    [SerializeField] GameObject gimmick;
    [SerializeField] GameObject planetGreen;
    [SerializeField] GameObject planetRed;

    private void Start()
    {
        GameObject gim = Instantiate(planetGreen, transform.position, Quaternion.identity);
        gim.transform.parent = gimmick.transform;
        int count = Random.Range(2, 5);
        GimmickCreate(count);
    }

    public void Delete()
    {
        int gimmickCount = transform.childCount;
        for (int i = 0; i < gimmickCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        Destroy(gameObject);
    }

    public void GimmickCreate(int gimmickCount)
    {
        GameObject gim;
        Vector3 pos = transform.position;
        for (int i = 0; i < gimmickCount; i ++)
        {
            pos.x = pos.x + Random.Range(-0.5f, 1);
            pos.y = pos.y + Random.Range(-0.5f, 1);
            pos.z = pos.z + Random.Range(0.5f, 0.75f);

            gim = Instantiate(planetRed, pos, Quaternion.identity);
            gim.transform.parent = gimmick.transform;
        }
    }

    private void Update()
    {
        int gimmickCount = transform.childCount;
        if(gimmickCount == 0)
        {
            Destroy(gameObject);
        }
    }
}
